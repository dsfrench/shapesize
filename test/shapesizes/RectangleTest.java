/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapesizes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dan
 */
public class RectangleTest {
    
    @Test
    public void testSetHeight() {
        Rectangle rectangle = new Rectangle();
        rectangle.setHeight(3);
        assertEquals(3, rectangle.getHeight());
    } 
    
    public void testSetWidth() {
        Rectangle rectangle = new Rectangle();
        rectangle.setWidth(4);
        assertEquals(4, rectangle.getWidth());        
    }
    
    public void testCalculateArea() {
        Rectangle rectangle = new Rectangle();
        rectangle.setWidth(4);
        rectangle.setHeight(5);
        assertEquals(20, rectangle.calculateArea(), 0.0001);
    }
}
