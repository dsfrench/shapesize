/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapesizes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dan
 */
public class TriangleTest {
    
    @Test
    public void testSetBase() {
        Triangle triangle = new Triangle();
        triangle.setBase(3);
        assertEquals(3, triangle.getBase());
    }
    
    @Test
    public void testSetHeight() {
        Triangle triangle = new Triangle();
        triangle.setHeight(5);
        assertEquals(5, triangle.getHeight());
    }

    @Test
    public void testCalculateArea() {
        Triangle triangle = new Triangle();
        triangle.setBase(3);
        triangle.setHeight(5);
        triangle.calculateArea();
        assertEquals(7.5 , triangle.getArea(), 0.00001);
    }

}
