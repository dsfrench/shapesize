/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapesizes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dan
 */
public class CircleTest {
    
    @Test
    public void testSetRadius() {
        Circle circle = new Circle();
        circle.setRadius(3);
        assertEquals(3, circle.getRadius());
    }
    
    @Test
    public void testCalculateDiameter() {
        Circle circle = new Circle();
        circle.setRadius(3);
        circle.calculateDiameter();
        assertEquals(6, circle.getDiameter());
    }
    
    @Test
    public void testCalculateCircumference() {
        Circle circle = new Circle();
        circle.setRadius(3);
        circle.calculateCircumference();
        assertEquals(18.85, circle.getCircumference(), 0.01);
    }
    
    @Test
    public void testCalculateArea() {
        Circle circle = new Circle();
        circle.setRadius(3);
        circle.calculateArea();
        assertEquals(28.27, circle.getArea(), 0.01);
    }
}
