/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapesizes;

/**
 *
 * @author Dan
 */
public class Circle extends Shape {
    
    private int radius;
    private int diameter;
    private double circumference;
    
    public void setRadius(int radius) {
        this.radius = radius;
    }
    
    public int getRadius() {
        return radius;
    }
    
    public void calculateDiameter() {
        diameter = radius * 2;
    }
    
    public int getDiameter() {
        return diameter;
    }
    
    public void calculateCircumference() {
        circumference = 2 * Math.PI * radius;
    }
    
    public double getCircumference() {
        return circumference;
    }
    
    public double calculateArea() {
        area = Math.PI*(radius * radius);
        return area;
    }
}
