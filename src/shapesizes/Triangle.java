/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapesizes;

/**
 *
 * @author Dan
 */
public class Triangle extends Shape {
    private int base;
    private int height;
    
    public void setBase(int base) {
        this.base = base;
    }
    
    public int getBase() {
        return base;
    }
    
    public void setHeight(int height) {
        this.height = height;
    }
    
    public int getHeight() {
        return height;
    }
    
    public void calculateArea() {
        area = (height * base) / 2.0;
    }
    
    public double getArea() {
        return area;
    }
}
