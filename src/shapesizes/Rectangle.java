/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapesizes;

/**
 *
 * @author Dan
 */
public class Rectangle extends Shape{
    private int height;
    private int width;

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }
    
    public void setWidth(int width) {
        this.width = width;
    }
    
    public int getWidth() {
        return width;
    }
    
    public double calculateArea() {
        area = height * width;
        return area;
    }
    
}
